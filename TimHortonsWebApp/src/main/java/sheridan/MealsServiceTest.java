package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("The dirk list is empty", types.isEmpty() == true);
	}

	@Test
	public void testDrinksException() {
		MealsService ms = new MealsService();
		assertTrue("The dirk list do not has any options", true);
	}

	@Test
	public void testDrinksBoundaryIn() {
		List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("The dirk list do has more than 3 elements", types.size()>3 == false);
	}

	@Test
	public void testDrinksBoundaryOut() {
		List<String> types = MealsService.getAvailableMealTypes(MealType.DRINKS);
		assertTrue("The dirk list do has more than 3 elements", types.size()<=1);
	}

}
